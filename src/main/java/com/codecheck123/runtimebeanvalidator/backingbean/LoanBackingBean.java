package com.codecheck123.runtimebeanvalidator.backingbean;

import java.math.BigDecimal;

import javax.validation.constraints.Min;

import com.codecheck123.runtimebeanvalidator.constraint.CONSTRAINT_TYPE;
import com.codecheck123.runtimebeanvalidator.constraint.RuntimeNumberConstraint;

public class LoanBackingBean {

	private BigDecimal loanOffer;
	
	@Min(value=500,message="Now you just wasting my time, what's wrong with you? minimum is 500")
	@RuntimeNumberConstraint(dependsOnField="loanOffer",constraintType=CONSTRAINT_TYPE.MAX, 
		message="Hey hey hey...What you think you doing?",fieldOnClass=LoanBackingBean.class)
	private BigDecimal loanTaken;
	
	public BigDecimal getLoanOffer() {
		return loanOffer;
	}
	public void setLoanOffer(BigDecimal loanOffer) {
		this.loanOffer = loanOffer;
	}
	public BigDecimal getLoanTaken() {
		return loanTaken;
	}
	public void setLoanTaken(BigDecimal loanTaken) {
		this.loanTaken = loanTaken;
	}
}
