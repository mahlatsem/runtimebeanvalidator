package com.codecheck123.runtimebeanvalidator.constraint;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.codecheck123.runtimebeanvalidator.validator.RuntimeValidator;

/**
 * Constraint for validating any Type extending from java.lang.Number at runtime
 * @author matt.mahlatse@gmail.com
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = RuntimeValidator.class)
public @interface RuntimeNumberConstraint {

	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
	String message() default "";
	String dependsOnField();
	Class<?> fieldOnClass();
	CONSTRAINT_TYPE constraintType();
}
