package com.codecheck123.runtimebeanvalidator.util;

import java.util.HashMap;
import java.util.Map;

public enum CustomerSessionCache {
	SESSION;
	
	private static final Map<Class<?>, Object> cache = new HashMap<Class<?>, Object>();
	
	public Object retrieveBackingBean(Class<?> storedClasses){
		return cache.get(storedClasses);
	}
	
	public Object storeBackingBean(Class<?> storedClasses, Object obj){
		return cache.put(storedClasses,obj);
	}
}
