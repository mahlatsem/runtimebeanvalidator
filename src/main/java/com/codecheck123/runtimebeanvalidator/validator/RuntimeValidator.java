package com.codecheck123.runtimebeanvalidator.validator;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.codecheck123.runtimebeanvalidator.constraint.CONSTRAINT_TYPE;
import com.codecheck123.runtimebeanvalidator.constraint.RuntimeNumberConstraint;
import com.codecheck123.runtimebeanvalidator.util.CustomerSessionCache;

public class RuntimeValidator implements ConstraintValidator<RuntimeNumberConstraint, Number>{
	
	private String dependsOnField;
	private CONSTRAINT_TYPE constrainType;	
	private Class<?> fieldOnClass;
	
	public void initialize(RuntimeNumberConstraint annotation) {
		dependsOnField = annotation.dependsOnField();
		constrainType = annotation.constraintType();
		fieldOnClass = annotation.fieldOnClass(); 
	}

	public boolean isValid(Number validationValue, ConstraintValidatorContext ctx) {
		
		Number dependencyValue = fetchDepensOnFieldValue(fieldOnClass);	
		boolean valid = false;
		switch(constrainType){
			case MIN:
				valid = (dependencyValue.longValue() <= validationValue.longValue());
				break;
			case MAX:
				valid = (dependencyValue.longValue() >= validationValue.longValue());
				break;
		}
		
		return valid;
	}
	
	private Number fetchDepensOnFieldValue(Class<?> fieldOnClass){
		Number dependencyValue = null;
		
		try {
			Method method = fieldOnClass.getMethod("get"+dependsOnField.substring(0, 1).toUpperCase()+dependsOnField.substring(1), null);			
			dependencyValue = (Number) method.invoke((fieldOnClass.cast(CustomerSessionCache.SESSION.retrieveBackingBean(fieldOnClass))), null);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return dependencyValue;
	}
}
