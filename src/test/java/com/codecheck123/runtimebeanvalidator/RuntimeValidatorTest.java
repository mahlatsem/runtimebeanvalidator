package com.codecheck123.runtimebeanvalidator;

import java.math.BigDecimal;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Test;
import static org.junit.Assert.*;

import com.codecheck123.runtimebeanvalidator.backingbean.LoanBackingBean;
import com.codecheck123.runtimebeanvalidator.util.CustomerSessionCache;

public class RuntimeValidatorTest {

	@Test
	public void testRuntimeValidator_MoreThanOffer(){
		
		//given
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		
		LoanBackingBean backingBean = new LoanBackingBean();
		backingBean.setLoanOffer(new BigDecimal("20000.00"));
		
		//to emphasize that this backing bean is stored into customer's session by whichever framework used before presenting offer to customer 
		CustomerSessionCache.SESSION.storeBackingBean(LoanBackingBean.class,backingBean);
		
		//when
		//customer inserts this on some front-end
		backingBean.setLoanTaken(new BigDecimal("50000.00")); 		
		Set<ConstraintViolation<LoanBackingBean>> constraintViolations = validator.validate(backingBean);
		
		//then
		assertEquals("Hey hey hey...What you think you doing?", constraintViolations.iterator().next().getMessage());		
	}
}
